typedef struct {
  unsigned id;
  int active;
  char *name;
} Cars;

Cars *make_cars(unsigned id) {
  
  Cars *stud;
  
  if ((stud = (Cars *)malloc(sizeof(Cars))) == NULL) {
    fprintf(stderr, "Failed to allocate Cars structure!\n");
    exit(EXIT_FAILURE);
  }
  stud->active = 0;
  stud->id = id;
  
  return stud;
}
