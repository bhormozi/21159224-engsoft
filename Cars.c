#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemas.h>
#include "Cars.h"
#define MAXCHARS 256
                           

int is_valid(const xmlDocPtr doc, const char *schema_filename)
{
  xmlDocPtr schema_doc = xmlReadFile(schema_filename, NULL, XML_PARSE_NONET);
  if (schema_doc == NULL) {
    /* the schema cannot be loaded or is not well-formed */
    return -1;
  }
  xmlSchemaParserCtxtPtr parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
  if (parser_ctxt == NULL) {
    /* unable to create a parser context for the schema */
    xmlFreeDoc(schema_doc);
    return -2;
  }
  xmlSchemaPtr schema = xmlSchemaParse(parser_ctxt);
  if (schema == NULL) {
    /* the schema itself is not valid */
    xmlSchemaFreeParserCtxt(parser_ctxt);
    xmlFreeDoc(schema_doc);
    return -3;
  }
  xmlSchemaValidCtxtPtr valid_ctxt = xmlSchemaNewValidCtxt(schema);
  if (valid_ctxt == NULL) {
    /* unable to create a validation context for the schema */
    xmlSchemaFree(schema);
    xmlSchemaFreeParserCtxt(parser_ctxt);
    xmlFreeDoc(schema_doc);
    return -4; 
  }
  int is_valid = (xmlSchemaValidateDoc(valid_ctxt, doc) == 0);
  xmlSchemaFreeValidCtxt(valid_ctxt);
  xmlSchemaFree(schema);
  xmlSchemaFreeParserCtxt(parser_ctxt);
  xmlFreeDoc(schema_doc);
  /* force the return value to be non-negative on success */
  return is_valid ? 1 : 0;
}

int main(int argc, char **argv)
{
  char buffer [ MAXCHARS ]; 

  xmlDocPtr doc = NULL;
  xmlNodePtr root_node = NULL, node = NULL , node1 = NULL ;
  {
    //While loop, in order to organise the priority of library

    int value = 1;
    
    while(value<=3)
      {
	printf("Sport car %d\n", value);
	value++;
      }
  }

  //Main title insisting of various cars with their unique details
  
  root_node = xmlNewNode(NULL, BAD_CAST "cars"); 
  xmlDocSetRootElement(doc, root_node);   
  node = xmlNewChild(root_node, NULL , BAD_CAST "SportCars", NULL);
  
  //getting the SportCars title
  printf ("Enter SportCars title:");
  fgets( buffer,MAXCHARS,stdin);
  
  node1 = xmlNewChild(node, NULL , BAD_CAST "name", BAD_CAST buffer);
  
  //getting the Assembly title
  printf ("Enter Assembly title:");
  fgets( buffer,MAXCHARS,stdin);
  
  xmlNewChild(node1,NULL, BAD_CAST "Assembly",BAD_CAST buffer);
  
  //getting the Designer title
  printf ("Enter Designer title:");
  fgets( buffer,MAXCHARS,stdin);
  
  xmlNewChild(node1,NULL ,  BAD_CAST "Designer",BAD_CAST buffer);
  
  //getting the Firstproduction title
  printf ("Enter Firstproduction title:");
  fgets( buffer,MAXCHARS,stdin);
  
  xmlNewChild(node1,NULL, BAD_CAST "Firstproduction",BAD_CAST buffer);
  
  //getting the Heightin title
  printf ("Enter Heightin title:");
  fgets( buffer,MAXCHARS,stdin);
  
  xmlNewChild(node1,NULL, BAD_CAST "Heightin",BAD_CAST buffer);
  
  node1 = xmlNewChild(node, NULL , BAD_CAST "BugattiVeyron", NULL);
  xmlNewChild(node1,NULL, BAD_CAST "Assembly", BAD_CAST "France");
  xmlNewChild(node1,NULL ,  BAD_CAST "Designer", BAD_CAST "Jozef Kaban");
  xmlNewChild(node1,NULL, BAD_CAST "Firstproduction", BAD_CAST "2005");
  xmlNewChild(node1,NULL, BAD_CAST "Heightin", BAD_CAST "45.6");
  node1 = xmlNewChild(node, NULL , BAD_CAST "MaseratiMC12", NULL);
  xmlNewChild(node1,NULL, BAD_CAST "Assembly", BAD_CAST "Italy");
  xmlNewChild(node1,NULL ,  BAD_CAST "Designer", BAD_CAST "Frank Stephenson");
  xmlNewChild(node1,NULL, BAD_CAST "Firstproduction", BAD_CAST "2004");
  xmlNewChild(node1,NULL, BAD_CAST "Heightin", BAD_CAST "47.4");
  node1 = xmlNewChild(node, NULL , BAD_CAST "Porsche991", NULL); 
  xmlNewChild(node1,NULL, BAD_CAST "Assembly", BAD_CAST "Germany");
  xmlNewChild(node1,NULL ,  BAD_CAST "Designer", BAD_CAST "Michael Mauer");
  xmlNewChild(node1,NULL, BAD_CAST "Firstproduction", BAD_CAST "2011");
  xmlNewChild(node1,NULL, BAD_CAST "Heightin", BAD_CAST "50.9");
  node1 = xmlNewChild(node, NULL , BAD_CAST "NissanGT-R", NULL); 
  xmlNewChild(node1,NULL, BAD_CAST "Assembly", BAD_CAST "Japan");
   xmlNewChild(node1,NULL ,  BAD_CAST "Designer", BAD_CAST "Shiro Nakamura");
   xmlNewChild(node1,NULL, BAD_CAST "Firstproduction", BAD_CAST "2007");
   xmlNewChild(node1,NULL, BAD_CAST "Heightin", BAD_CAST "54");
   node1 = xmlNewChild(node, NULL , BAD_CAST "SaleenS7", NULL);
   xmlNewChild(node1,NULL, BAD_CAST "Assembly", BAD_CAST "United States");
   xmlNewChild(node1,NULL ,  BAD_CAST "Designer", BAD_CAST "Steve Saleen");
   xmlNewChild(node1,NULL, BAD_CAST "Firstproduction", BAD_CAST "2000");
   xmlNewChild(node1,NULL, BAD_CAST "Heightin", BAD_CAST "41");
   node = xmlNewChild(root_node, NULL , BAD_CAST "Formula1Drivers", NULL); 
   node1 = xmlNewChild(node, NULL , BAD_CAST "KimiRaikkonen", NULL);
   xmlNewChild(node1,NULL, BAD_CAST "Nationality", BAD_CAST "Finish");
   xmlNewChild(node1,NULL ,  BAD_CAST "Team", BAD_CAST "Lotus Renault");
   node1 = xmlNewChild(node, NULL , BAD_CAST "LewisHamilton", NULL); 
   xmlNewChild(node1,NULL, BAD_CAST "Nationality", BAD_CAST "British");
   xmlNewChild(node1,NULL ,  BAD_CAST "Team", BAD_CAST "Mercedes");
   xmlSaveFormatFileEnc("sportcars.xml", doc, "UTF-8", 1);
   
   is_valid(doc, "cars.xsd");
   
   xmlFreeDoc(doc);
   xmlCleanupParser();
   
   return(0);
}


