#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>


static void print_cars(xmlNode * a_node)
{
  xmlNode *cur_node = NULL;
  xmlChar *value = NULL;
  
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      value = xmlNodeGetContent(cur_node);
      printf("%s: %s\n", cur_node->name, value);
      xmlFree(value);
    }
  }
}

static void search_cars(xmlNode *a_node, xmlChar *id)
{
  xmlNode *cur_node = NULL;
  xmlChar *value = NULL;
  
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      value = xmlNodeGetContent(cur_node);
      if (xmlStrEqual(value, id)) {
        print_cars(cur_node);
        xmlFree(value);
        break;
      }
      xmlFree(value);
    }
    search_cars(cur_node->children, id);
  }
}

int main(int argc, char **argv)
{
  xmlDoc *doc = NULL;
  xmlNode *root_element = NULL;
  
  if (argc != 3)
    return(1);
  
  doc = xmlReadFile(argv[1], NULL, 0);
  
  if (doc == NULL) {
    printf("error: could not parse file %s\n", argv[1]);
  }
  
  root_element = xmlDocGetRootElement(doc);
  search_cars(root_element, BAD_CAST argv[2]);
  xmlFreeDoc(doc);
  xmlCleanupParser();
  
  return 0;
}

